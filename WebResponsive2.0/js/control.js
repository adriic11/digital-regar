var output = document.getElementById("rango");
var slider = document.getElementById("customRange").oninput = function () {

	var value = (this.value-this.min)/(this.max-this.min)*100;

	output.innerHTML = this.value;
};

var output1 = document.getElementById("rango1");
var slider1 = document.getElementById("customRange1").oninput = function () {

    var value1 = (this.value-this.min)/(this.max-this.min)*100;

    output1.innerHTML = this.value;
};

(function() {
      'use strict';
      window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            form.classList.add('was-validated');
          }, false);
        });
      }, false);
    })();