<?php
	require 'conexion.php';
	
	if ($_SERVER["REQUEST_METHOD"] == "POST") {

		$planta = $_POST["tipoPlanta1"];
		$fechaInicio = $_POST["fechaInicio1"];
	  	$frecuencia = $_POST["frecuenciaRiego1"];
	  	$duracion = $_POST["duracionRiego1"];
	
		$statement = $conexion->prepare("INSERT INTO control_zona2 (Nombre, Fecha, Frecuencia, Humedad_min)  VALUES(?, ?, ?, ?)"); //insertar query
		
		$statement->bind_param('sssi', $planta, $fechaInicio, $frecuencia, $duracion); //bind values y ejecutar query
		
		if($statement->execute()){
			echo  "<script type='text/javascript'>";
			echo "window.close();";
			echo "</script>";
		}
		else {
			print $conexion->error; 
		}
	}
?>
