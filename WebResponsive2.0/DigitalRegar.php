<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Digital Regar</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" 
  integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <script src="script/script.js"></script>
 
</head>
<body>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" 
  integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

  <!-- BARRA DE NAVEGACIÓN -->
  <nav class="navbar navbar-light fixed-top" style="background: #2FCD4A;">
    <div class="container-fluid">
      <a class="navbar-brand" href="#">
        DIGITAL REGAR
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar" 
      aria-controls="offcanvasNavbar">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasNavbar" aria-labelledby="offcanvasNavbarLabel">
        <div class="offcanvas-header" 
        style="background-image: linear-gradient( 117deg,  rgba(123,216,96,1) 39.2%, rgba(255,255,255,1) 156.2% );">
          <h5 class="offcanvas-title" id="offcanvasNavbarLabel">Digital Regar</h5>
          <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body">
          <ul class="navbar-nav justify-content-end flex-grow-1 pe-3">
            <li class="nav-item">
              <a class="nav-link" aria-current="page" href="ZonaRiego.php">ZonaRiego</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" aria-current="page" href="WikiPlanta.php">WikiPlanta</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>

  <!--Container imagen-->
  <div class="container-fluid  row align-items-center md-4 mt-5" style="height: 350px;" >
    <div>
      <img src="img/logo.png" class="mx-auto rounded d-block" alt="">
    </div>
  </div>

  <!--Container con las tarjetas-->    
  <div class="container mb-5">
    <!--tarjeta 1-->
    <div class="row justify-content-evenly">
      <div class="col-4">
        <div class="card border-2 rounded-3 overflow-hidden" style="box-shadow: 0 2px 5px 0 rgb(0, 0, 0, .16), 0 2px 10px 0 rgba(0, 0, 0, .12); height:20rem;">
          <div class="card-header card-header-first rounded" style="box-shadow: 1px 7px 15px #a2a2a2; background: rgb(148,187,233);
	        background-image: linear-gradient( 117deg,  rgba(123,216,96,1) 39.2%, rgba(255,255,255,1) 156.2% ); height: 100px;"> 
            <h1 class="card-header-title mb-3 mt-1 text-black text-center" style="font-family: Copperplate; font-size: 50px;"> ZonaRiego </h1>
          </div>
          <div class="card-body text-center">
            <p class="card-text">
              Configura las zonas de riego 
              <br>
              de tus plantas.
              <br>
              Ajusta la hora, el día y el modo de riego. 
              <br>
              Visualiza el desarrollo de tus plantas.
            </p>
            <hr>
            <button class="btn btn-lg btn-outline-success"  onclick="location.href='ZonaRiego.php'"> Entrar </button>
          </div> 
        </div>
      </div>  
      <!--tarjeta 2--> 
      <div class="col-4">
        <div class="card border-2 rounded-3 overflow-hidden" style="box-shadow: 0 2px 5px 0 rgb(0, 0, 0, .16), 0 2px 10px 0 rgba(0, 0, 0, .12); height:20rem;">
          <div class="card-header card-header-first rounded" style="box-shadow: 1px 7px 15px #a2a2a2; background: rgb(148,187,233);
	        background-image: linear-gradient( 117deg,  rgba(123,216,96,1) 39.2%, rgba(255,255,255,1) 156.2% ); height: 100px;"> 
            <h1 class="card-header-title mb-3 mt-1 text-black text-center" style="font-family: Copperplate; font-size: 50px;"> WikiPlanta </h1>
          </div>
          <div class="card-body text-center">
            <p class="card-text"> 
              Características principales 
              <br>
              de todas las plantas de interior.
              <br>
              Descripción sobre ellas y sus mejores 
              <br>
              formas de cuidado. 
            </p>
            <hr>
            <div class="text-center">
              <button class="btn btn-lg btn-outline-success" onclick="location.href='WikiPlanta.php'" > Entrar </button>
            </div>
          </div> 
        </div>
      </div>

    </div>
  </div>

</body>
</html> 