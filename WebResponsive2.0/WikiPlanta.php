<?php
	include 'conexion.php'
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <title>Digital Regar</title>
  <style>
    .ho:hover {
      text-shadow: none !important;
      box-sizing: border-box !important;
      cursor: pointer !important;
      transition: all 0.3s ease !important;
      -webkit-trasnform: scale(1.1) !important;
      transform: scale(1.1) !important;
      z-index: 2;
    }
  </style>
</head>
<body>
  <!-- BARRA DE NAVEGACIÓN -->
  <nav class="navbar navbar-light navbar-expand-lg" style="background-color: #2FCD4A;">
      <div class="container-fluid">
        <a class="navbar-brand" href="DigitalRegar.php">
             Digital Regar
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="ZonaRiego.php">Zona Riego</a>
            </li>
          </ul>
        </div>
      </div>
  </nav>

  <main>
    <!--Logo página web-->
    <div class="container mt-5">
      <div class="row">
        <div class="col d-flex justify-content-center">
          <img src="img/logo.png" class="img-fluid" alt="..." >
        </div>
      </div>
    </div>

    <!--Barra de búsqueda-->
    <div class="container mt-3">
      <div class="row ">
        <div class="col-12">
          <nav class="navbar-expand-lg navbar-light ">
            <div class="container-fluid">
              <form class="d-flex" action="WikiPlanta.php">
                <input class="form-control me-3" type="search" placeholder="Buscar planta..." aria-label="Search" name="planta" value="<?php echo $_REQUEST['planta']??'';?>">
                <input type="hidden" name="modulo" value="plantas">
                <button class="btn btn-outline-success " type="submit">Buscar</button>
                <button class="btn btn-outline-success mx-1" type="button" onclick="location.href='WikiPlanta.php'">Volver</button>
              </form>
            </div>
          </nav>
        </div>
      </div>
      <!--Tarjeta con la informacion de las plantas-->  
      <div class="row mb-5">   
        <?php
            $where = " where 1=1 ";
            $planta = mysqli_real_escape_string($conexion,$_REQUEST['planta']??'');
            if( empty($planta)==false ){
              $where="and nombre LIKE '%$planta%'";
            }
            //QUERY a la Base de datos
            $sql = "SELECT Nombre, Descripcion, Temperatura, Riego, Imagen FROM planta WHERE nombre like '%$planta%' order by nombre;";
            // resultado de la quaery de la BD
            $resultSet=mysqli_query($conexion,$sql);

            while(($row=mysqli_fetch_row($resultSet))!=null){
        ?>
        <div class="col-lg-4 col-md-6 col-sm-12 card-group mt-5">
          <div class="card mt-1 me-3 ho " >
            <img src="img/<?php echo $row[4];?>" class="card-img-top" alt="..." height="300">
            <div class="card-body">
              <h3 class="card-title" style="font-family: Copperplate;"><?php echo $row[0];?></h3>
              <p class="card-text"><?php echo $row[1];?></p>
              <h5 class="card-title" style="font-family: Copperplate;">Cuidados</h5>
              <hr class="mb-1">
              <p class="card-text"><?php echo $row[2];?></p>
              <p class="card-text"><?php echo $row[3];?></p>
            </div>
          </div>
        </div> 
        <?php
          }
          mysqli_close($conexion);                
        ?> 
      </div>
    </div>
  </main>
</body>
</html>