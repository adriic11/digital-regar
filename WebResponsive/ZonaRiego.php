<?php
    include 'conexion.php'
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">   
    <title>ZonaRiego</title>
</head>
<body>  
  <nav class="navbar navbar-light navbar-expand-lg" style="background: #2FCD4A;">
        <div class="container-fluid">
          <a class="navbar-brand" href="DigitalRegar.php">Digital Regar</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">      
              <li class="nav-item">
                <a class="nav-link" href="WikiPlanta.php">WikiPlanta</a>
              </li>
            </ul>
          </div>
        </div>
  </nav>

    <nav>
      <div class="nav nav-tabs justify-content-center mt-5 mx-5" id="nav-tab" role="tablist">
        <button class="nav-link active" id="nav-zona1-tab" data-bs-toggle="tab" data-bs-target="#nav-zona1" 
        type="button" role="tab" aria-controls="nav-zona1" aria-selected="true">Zona Riego 1</button>
        <button class="nav-link" id="nav-zona2-tab" data-bs-toggle="tab" data-bs-target="#nav-zona2" 
        type="button" role="tab" aria-controls="nav-zona2" aria-selected="false">Zona Riego 2</button> 
      </div>
    </nav>

    <div class="tab-content " id="nav-tabContent">
      <div class="tab-pane fade show active" id="nav-zona1" role="tabpanel" aria-labelledby="nav-zona1-tab">
        <div class="container mt-5 mb-5">
            <div class="row justify-content-between">
             <?php
                
                //query para conseguir os datos de la base de datos de los sensores, limitado al ultimo valor((cambiar con parametros de la BD))
                $query = sprintf("SELECT Fecha, Humedad_tierra, Humedad_ambiente, Temperatura_ambiente, Volumen_consumido, Volumen_tanque FROM sensor_zona1 ORDER BY Fecha DESC LIMIT 1");

                //execute query
                $result = mysqli_query($conexion,$query);

                // resultado de la quaery de la BD
                while(($row=mysqli_fetch_row($result))!=null){
              ?>
                <div class="col-4">
                    <div class="card border-2 rounded-3 overflow-hidden" style="box-shadow: 0 10px 30px 0 rgb(0, 0, 0, .16), 0 10px 30px 0 rgba(0, 0, 0, .12); width: 30rem;">
                        <img class="card-img" src="img/planta.jfif" alt="Card image">
                        <div class="card-img-overlay">                  
                            <h1 class="card-title mb-4 mt-1 text-black text-center" style="font-family: Copperplate; font-size: 50px;"> Zona Riego 1</h1>   
                            <hr class="my-4">
                                <div>            
                                    <h5 class="my-4" style="font-family: Copperplate; font-size: 25px;">Datos Ambientales</h5>
                                    <p>Humedad Tierra: <?php echo $row[1];?> %</p>
                                    <p>Humedad Ambiental: <?php echo $row[2];?> % </p>
                                    <p>Temperatura Ambiental: <?php echo $row[3];?> C</p>
                                </div>
                                <hr class="my-3">
                                <div>            
                                    <h5 class="my-4" style="font-family: Copperplate; font-size: 25px;">Estado actual zona Riego</h5>
                                    <p>Volumen consumido: <?php echo $row[4];?> Litros</p>
                                    <p>Volumen tanque: <?php echo $row[5];?> Litros</p>
                                </div>
                                <hr class="mt-3 mb-4">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-outline-success" data-bs-toggle="modal" data-bs-target="#exampleModal" >
                                    Configurar Zona Riego
                                </button>
                                
                        </div>
                    </div>
                    <?php
                        }
                                          
                    ?>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content pb-1">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">ZonaRiego 1</h5>
                                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="controlZona1.php" method="post" target="_blank" class="needs-validation" novalidate>
                                    <div class="row g-3 mb-3">              
                                        <div class="col-12">
                                            <h6>Tipo Planta</h6>
                                            <input id="tipoPlanta" name="tipoPlanta" type="text" class="form-control" required>
                                            <div class="valid-feedback"></div>
                                            <div class="invalid-feedback"></div>
                                        </div>
                                        <div class="col-12">
                                            <h6>Configuracion Riego Programado</h6>
                                            <div class="input-group">
                                                <span style="width:200px" class="input-group-text" id="basic-addon1">Fecha Inicio</span>
                                                <input name="fechaInicio" type="datetime-local" class="form-control" id="fechaInicio" aria-describedby="basic-addon1" required>
                                                <div class="valid-feedback"></div>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                            <div class="input-group ">
                                                <span style="width:200px" class="input-group-text" id="basic-addon2">Frecuencia</span>
                                                <div class="form-control">
                                                    <div class="form-check">
                                                        <input id="diario" name="frecuenciaRiego" type="radio" class="form-check-input" value="diario" required>
                                                        <label class="form-check-label" for="diario">Diario</label> 
                                                    </div>
                                                    <div class="form-check">
                                                        <input id="semanal" name="frecuenciaRiego" type="radio" class="form-check-input" value="semanal" required>
                                                        <label class="form-check-label" for="semanal">Semanal</label>
                                                        <div class="invalid-feedback"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="input-group ">
                                                <span style="width:200px" for="customRange" class="input-group-text" id="basic-addon2">Necesidad de agua</span>
                                                <div class="form-control">
                                                    <div class="form-check"> 
                                                        <div class="range text-center">    
                                                            <div class="value-container">
                                                                <span id="rango">0</span>
                                                            </div>
                                                            <input class="form-range" value="0" min="0" max ="100" name="duracionRiego" type="range" id="customRange" required>        
                                                            <div class="invalid-feedback">No valido</div> 
                                                            <div class="valid-feedback">Campo valido</div>
                                                        </div>
                                                    </div>    
                                                </div>
                                            </div>
                                        </div>              
                                    </div>
                                    <button class="btn btn-outline-success" id="btnZona1" type="submit" value="submit" style="width: 20rem;">Enviar</button>
                                    <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal" style="width: 135px">Close</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>     
                <div class="col-lg-6">   
                    <canvas id="myChart" height="300" ></canvas>               
                </div>
            </div>
        </div>
      </div>
      <div class="tab-pane fade" id="nav-zona2" role="tabpanel" aria-labelledby="nav-zona2-tab">
         <div class="container mt-5 mb-5">
            <div class="row justify-content-between">
                <?php
                    
                    //query para conseguir os datos de la base de datos de los sensores, limitado al ultimo valor((cambiar con parametros de la BD))
                    $query = sprintf("SELECT Time, Humedad_tierra, Humedad_ambiente, Temperatura_ambiente, Volumen_consumido, Volumen_tanque FROM sensor_zona2 ORDER BY Time DESC LIMIT 1");

                    //execute query
                    $result = mysqli_query($conexion,$query);

                    // resultado de la quaery de la BD
                    while(($row=mysqli_fetch_row($result))!=null){
                ?>
                <div class="col-4">
                    <div class="card border-2 rounded-3 overflow-hidden" style="box-shadow: 0 15px 30px 0 rgb(0, 0, 0, .16), 0 2px 10px 0 rgba(0, 0, 0, .12); width: 30rem;">
                        <img class="card-img" src="img/planta.jfif" alt="Card image">
                        <div class="card-img-overlay">                  
                            <h1 class="card-title mb-4 mt-1 text-black text-center" style="font-family: Copperplate; font-size: 50px; "> Zona Riego 2</h1>   
                            <hr class="my-4">
                                 <div>            
                                    <h5 class="my-4" style="font-family: Copperplate; font-size: 25px;">Datos Ambientales</h5>
                                    <p>Humedad Tierra: <?php echo $row[1];?> % </p>
                                    <p>Humedad Ambiental: <?php echo $row[2];?> % </p>
                                    <p>Temperatura Ambiental: <?php echo $row[3];?> C </p>
                                </div>
                                <hr class="my-3">
                                <div>            
                                    <h5 class="my-4" style="font-family: Copperplate; font-size: 25px;">Estado actual zona Riego</h5>
                                    <p>Volumen consumido: <?php echo $row[4];?> Litros </p>
                                    <p>Volumen tanque: <?php echo $row[5];?> Litros </p>
                                </div>
                                <hr class="mt-3 mb-4">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-outline-success" data-bs-toggle="modal" data-bs-target="#exampleModal1" >
                                    Configurar Zona Riego
                                </button>
                                
                        </div>
                    </div>
                    <?php
                    }
                        mysqli_close($conexion);                  
                    ?>
                    <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content pb-1">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel1">ZonaRiego 2</h5>
                                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="controlZona2.php" method="post" target="_blank" class="needs-validation" novalidate>
                                    <div class="row g-3 mb-3">              
                                        <div class="col-12 " >
                                            <h6>Tipo Planta</h6>
                                            <input id="tipoPlanta1" name="tipoPlanta1" type="text" class="form-control" required>
                                            <div class="valid-feedback"></div>
                                            <div class="invalid-feedback"></div>
                                        </div>
                                        <div class="col-12">
                                            <h6>Configuracion Riego Programado</h6>
                                            <div class="input-group">
                                                <span style="width:200px" class="input-group-text" id="basic-addon1">Fecha Inicio</span>
                                                <input name="fechaInicio1" type="datetime-local" class="form-control" id="fechaInicio1" aria-describedby="basic-addon1" required>
                                                <div class="valid-feedback"></div>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                            <div class="input-group ">
                                                <span style="width:200px" class="input-group-text" id="basic-addon2">Frecuencia</span>
                                                <div class="form-control">
                                                    <div class="form-check">
                                                        <input id="diario1" name="frecuenciaRiego1" type="radio" class="form-check-input" value="diario" required>
                                                        <label class="form-check-label" for="diario">Diario</label> 
                                                    </div>
                                                    <div class="form-check">
                                                        <input id="semanal" name="frecuenciaRiego1" type="radio" class="form-check-input" value="semanal" required>
                                                        <label class="form-check-label" for="semanal">Semanal</label>
                                                        <div class="invalid-feedback"></div>
                                                    </div>
                                                </div>

                                            </div>
                                           <div class="input-group">
                                                <span style="width:200px" for="customRange1" class="input-group-text" id="basic-addon2">Necesidad de agua</span>
                                                <div class="form-control">
                                                    <div class="form-check"> 
                                                        <div class="range text-center">    
                                                            <div class="value-container">
                                                                <span id="rango1">0</span>
                                                            </div>
                                                            <input class="form-range" value="0" min="0" max ="100" name="duracionRiego1" type="range" id="customRange1" required>        
                                                            <div class="invalid-feedback">No valido</div> 
                                                            <div class="valid-feedback">Campo valido</div>
                                                        </div>
                                                       
                                                    </div>    
                                                </div>
                                            </div>
                                        </div>              
                                    </div>
                                    <button class="btn btn-outline-success" id="btnZona2" type="submit" value="submit" style="width: 20rem;">Enviar</button>
                                    <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal" style="width: 135px">Close</button>  
                                </form>
                            </div>
                        </div>
                    </div>
                    </div>  
                </div>
                <div class="col-lg-6">
                    <canvas id="myChart2" height="300" ></canvas>
                </div>
            </div>
         </div>
      </div>
    </div>

</body>
</html>
<script src="js/control.js"></script>
<script type="text/javascript" src="myChart/js/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" 
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script src="js/graficos.js"></script>
