//Funcion para añadir valor al input range
var output = document.getElementById("rango");
var slider = document.getElementById("customRange").oninput = function () {

	var value = (this.value-this.min)/(this.max-this.min)*100;

	output.innerHTML = this.value;
};
var output1 = document.getElementById("rango1");
var slider1 = document.getElementById("customRange1").oninput = function () {

    var value1 = (this.value-this.min)/(this.max-this.min)*100;

    output1.innerHTML = this.value;
};

//Funcion para validar los datos de los input del form
(function() {
      'use strict';
      window.addEventListener('load', function() {
        
        var forms = document.getElementsByClassName('needs-validation');
        
        var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            form.classList.add('was-validated');
          }, false);
        });
      }, false);
    })();