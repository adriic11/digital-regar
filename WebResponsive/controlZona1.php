<?php
	require 'conexion.php';
	
	if ($_SERVER["REQUEST_METHOD"] == "POST") {

		$planta = $_POST["tipoPlanta"];
		$fechaInicio = $_POST["fechaInicio"];
	  	$frecuencia = $_POST["frecuenciaRiego"];
	  	$duracion = $_POST["duracionRiego"];
	
		$statement = $conexion->prepare("INSERT INTO control_zona1 (Nombre, Fecha, Frecuencia, Humedad_min)  VALUES(?, ?, ?, ?)"); //query para insertar los datos
		
		$statement->bind_param('sssi', $planta, $fechaInicio, $frecuencia, $duracion); //bind values y ejecutar la query
		
		if($statement->execute()){
			echo  "<script type='text/javascript'>";
			echo "window.close();";
			echo "</script>";
		}
		else {
			print $conexion->error; 
		}
	}
?>
