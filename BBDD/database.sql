CREATE DATABASE Digital_Regar;

USE Digital_Regar;

CREATE TABLE planta
(
	IdPlanta int AUTO_INCREMENT PRIMARY KEY,
	Nombre varchar(50),
	Descripcion varchar(325),
	Temperatura varchar(225),
	Riego varchar(225),
	Imagen varchar(50)
);

CREATE TABLE sensores_zona1
(
	IdSensores int AUTO_INCREMENT PRIMARY KEY,
	Fecha timestamp,
	Humedad_tierra float,
	Humedad_ambiente float,
	Temperatura_ambiente float,
	Volumen_consumido float,
	Volumen_tanque float
);

CREATE TABLE sensores_zona2
(
	IdSensores int AUTO_INCREMENT PRIMARY KEY,
	Fecha timestamp,
	Humedad_tierra float,
	Humedad_ambiente float,
	Temperatura_ambiente float,
	Volumen_consumido float,
	Volumen_tanque float
);

CREATE TABLE control_zona1
(
	IdControl int AUTO_INCREMENT PRIMARY KEY,
	Fecha timestamp,
	Frecuencia tinyint(1),
	Humedad_min float,
	Manual tinyint(1),
	Estado_bomba tinyint(1)
);

CREATE TABLE control_zona2
(
	IdControl int AUTO_INCREMENT PRIMARY KEY,
	Fecha timestamp,
	Frecuencia tinyint(1),
	Humedad_min float,
	Manual tinyint(1),
	Estado_bomba tinyint(1)
);

