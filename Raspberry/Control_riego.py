import Adafruit_DHT
import RPi.GPIO as GPIO
import time

sensor = Adafruit_DHT.DHT11
GPIO.setmode(GPIO.BCM)

pinTemp_Hum = 4
pinHumTierra =  17

GPIO.setup(pinHumTierra, GPIO.IN)

def medirHumTierra():
    print("Humedad de la tierra:")
    valor = GPIO.input(17)
    if valor == 0:
        print("Seco")
    else:
        print("Humedo")
        
def medirTempHum():
     print("Humedad y Temperatura:")
     humedad, temperatura = Adafruit_DHT.read_retry(sensor, pinTemp_Hum)
     print('Temp={0:0.1f}*C Humidity={1:0.1f}%'.format(temperatura, humedad))
    
while True:
     medirTempHum()
     medirHumTierra()
     time.sleep(5)