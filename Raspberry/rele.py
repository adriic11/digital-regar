import RPi.GPIO as GPIO
import time

RelayPin = 17    # pin 17

def setup():
	GPIO.setmode(GPIO.BOARD)       
	GPIO.setup(RelayPin, GPIO.OUT)
	GPIO.output(RelayPin, GPIO.LOW)

def loop():
	while True:
		print (rele on)
		GPIO.output(RelayPin, GPIO.LOW) # luz verde encendida
		time.sleep(0.5)                
		print (rele off)
		GPIO.output(RelayPin, GPIO.HIGH) # Nivel alto, el relé está activo
		time.sleep(0.5)                  # El contacto normalmente abierto está energizado, la luz roja está encendida

def destroy():
	GPIO.output(RelayPin, GPIO.LOW)
	GPIO.cleanup()                     

if __name__ == '__main__':     
	setup()
	try:
		loop()
	except KeyboardInterrupt:  # Cuando le demos a Ctrl + C el proceso hijo acabará
		destroy()