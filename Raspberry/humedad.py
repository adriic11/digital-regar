import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

pin =  17

GPIO.setup(pin, GPIO.IN)

def medirAgua():
    valor = GPIO.input(17)
    if valor == 0:
        print("Seco")
    else:
        print("Humedo")
        
print("Nivel de agua")

while True:
    medirAgua()
    time.sleep(2)
            