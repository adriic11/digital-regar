import time
import datetime
from datetime import date
import MySQLdb
import Adafruit_DHT
import RPi.GPIO as GPIO
import time

db = MySQLdb.connect(host="localhost", user="root", passwd="1234", db="digital_regar")  #conecta con MySQL
cur = db.cursor()  #crea el cursor para las peticiones de MySQL

#inicializa los sensores
sensor = Adafruit_DHT.DHT11
GPIO.setmode(GPIO.BCM)
pinTemp_Hum = 4
pinHumTierra =  17
GPIO.setup(pinHumTierra, GPIO.IN)

while True:
	valor = GPIO.input(17) #obtiene el valor del sensor de humedad de tierra
	humedad, temperatura = Adafruit_DHT.read_retry(sensor, pinTemp_Hum) #obtiene el valor del sensor de humedad y temperatura ambiente
	time.sleep(5)

	cur.execute('''INSERT INTO sensores (Humedad1,Temperatura,Humedad) values (%s, %s, %s);''', (valor,temperatura,humedad)) #inserta los valores en la bbdd
	db.commit()
