# Configuracion del puerto GPIO al cual esta conectado (GPIO 23)
pin = 23

# Configuracion MySQL
MYSQL_SERVIDOR = "localhost"
MYSQL_BD = "digital_regar"
MYSQL_USUARIO = "root"
MYSQL_CONTRASENA = "1234"

# Carga la configuracion local desde el archivo config_local.py
try:
	from config_local import *
except RuntimeError as error:
	print("ERROR: " + error.args[0])
