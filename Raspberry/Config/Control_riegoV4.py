from ast import While
import pymysql
import time
from datetime import datetime, timedelta
import Adafruit_DHT
import RPi.GPIO as GPIO
import math
import busio
import digitalio
import board
from MCP3008 import MCP3008


dia_ahora = datetime.now()
min_siguiente = dia_ahora + timedelta(minutes=30) 
min_anterior = dia_ahora - timedelta(minutes=30)
siguiente_semana = dia_ahora + timedelta(7)
siguiente_dia = dia_ahora + timedelta(1)
pinBomba1 = 21
pinBomba2 = 20



class Zona1:
    def __init__(self): #Conexion con la base de datos
        self.connection = pymysql.connect(host='localhost', user='root', password='1234', db='Digital_Regar')  #conecta con MySQL
        self.cursor = self.connection.cursor()  #crea el cursor para las peticiones de MySQL
        print("Conexion Zona 1 establecida")
        
    def select_fecha(self): #Función que saca la fecha de riego elegida por el cliente
        sql = 'SELECT Fecha FROM control_zona1 ORDER BY IdControl DESC LIMIT 1'
        try:
            self.cursor.execute(sql)
            fecha = self.cursor.fetchone()
            if (fecha[0] >= min_anterior and fecha[0] <= min_siguiente):  #Intervalo de una hora para el riego
                print("Fecha correcta para el riego.")
                time.sleep(1)
                return True
            else:
                print("Fecha incorrecta para el riego.")
                time.sleep(1)
                return False

        except Exception as e:
            raise

    def select_humedad_min(self): #Coge el ultimo valor de la humedad de tierra de la zona 1 elegida por el usuario
        sql =  'SELECT Humedad_min FROM control_zona1 ORDER BY IDControl DESC LIMIT 1'

        try:
            self.cursor.execute(sql)
            hum_min = self.cursor.fetchone()
            return hum_min[0]
        
        except Exception as e:
            raise

    def update_frecuencia_riego(self): #Coge el ultimo valor de la fecha introducida por el cliente
        sql = 'SELECT Frecuencia FROM control_zona1 ORDER BY IdControl DESC LIMIT 1'
        sql1 = 'UPDATE control_zona1 SET Fecha = %s ORDER BY IdControl DESC LIMIT 1'

        try:
            self.cursor.execute(sql)
            frecuencia = self.cursor.fetchone()
           #La base de datos devuelve un varchar, dependiendo del input que eliga el usuario en la web -> Frecuencia Semanal devuelve 'semanal' Diaro devuelve 'diario'
            if(frecuencia[0] == 'diario'): #Si la frecuencia de riego elegida por el cliente es diario cambia la fecha para el proximo riego
                val = (siguiente_dia)
                self.cursor.execute(sql1,val) #Ejecuta la query
                self.connection.commit()
                return
                
            elif(frecuencia[0] == 'semanal'): #Si la frecuencia de riego elegida por el cliente es semanal cambia la fecha para el proximo riego
                val = (siguiente_semana)
                self.cursor.execute(sql1,val) #Ejecuta la query
                self.connection.commit()
                return
        
        except Exception as e:
            raise

    def comparar_humedad(ref):
        print("Humedad de la planta = ",zona1.HumedadTierra())
        if(zona1.HumedadTierra() < zona1.select_humedad_min()):
            print("Humedad de la planta correcta para regar.")
            time.sleep(1)
            return True
        else:
            print("Humedad de la planta suficiente, no hace falta regar.")
            time.sleep(1)
            return False    
    
    def setup(self):
        GPIO.setmode(GPIO.BCM)       
        GPIO.setup(pinBomba1, GPIO.OUT)
        GPIO.output(pinBomba1, GPIO.LOW)
        
    def activar_bomba(self):
        
        zona1.setup()
        
        print ("Bomba activada.")
        GPIO.output(pinBomba1, GPIO.LOW) # luz verde encendida
        time.sleep(3)                
        print ("Bomba desactivada.")
        GPIO.output(pinBomba1, GPIO.HIGH) # Nivel alto, el relé está activo
        time.sleep(5)                  # El contacto normalmente abierto está energizado, la luz roja está encendida
                                

    def control_bomba(self):
        val = comun.VolumenTanqueInicial()
        print("Volumen tanque inicial = ",val)
        time.sleep(2)
        if(val > (0.20)): #Si tiene suficiente agua el tanque entra en la condicion
            print("Volumen suficiente en el tanque.")
            time.sleep(2)
            while(zona1.comparar_humedad() == True and zona1.select_fecha() == True):
                print("Regando..")
                zona1.activar_bomba() #Activa la bomba
                zona1.update_frecuencia_riego() # Cambia la fecha para el proximo riego al acabar de regar       
                val2 = comun.VolumenTanqueFinal()
                volumen = val - val2
                print("Volumen consumido en el riego = ",volumen)
                sql = 'UPDATE sensores_Zona1 SET Volumen_consumido = %s ORDER BY IdSensores DESC LIMIT 1;'
                self.cursor.execute(sql,volumen)
                self.connection.commit()
                time.sleep(10)
        else:
            zona1.update_frecuencia_riego()
            return        

        print("Fecha para el siguiente riego actualiazdo.")
      

    def HumedadTierra(self): #Coge el valor del sensor de humedad de la tierra
   
        adc = MCP3008(0)
        humedad = adc.read( channel = 0 ) #Recoge el valor del  sensor
        #valor  = (-0.147)*(humedad) + 147.1
        valor  = (-0.17)*(humedad) + 171.4 #Convertimos el valor entre 0-100
        return valor
        


    def InsertarValores(self):
        #Inserta los valores de Humedad_tierra y Volumen_tanque a la bbdd
        Volumen = comun.VolumenTanqueInicial()
        HumedadTierra = zona1.HumedadTierra()
        sql = 'INSERT INTO sensores_Zona1(Humedad_tierra,Volumen_tanque) VALUES (%s,%s)'
        
        val = (HumedadTierra,Volumen)
        self.cursor.execute(sql,val)
        self.connection.commit()

        zona1.Update_sensorAmbiente()
        

    def Update_sensorAmbiente(self):
        #Inserta el valor del sensor ambiente a la bbdd, hago un update ya que el sensor me devuelve dos valores para una misma variable, por lo que no me deja hacerle un insert
        #Por lo que hago el insert y luego el update de la ultima fila
        #val = comun.SensorAmbiente()
        val = (30,25)
        sql = 'UPDATE sensores_Zona1 SET Humedad_ambiente = %s, Temperatura_ambiente = %s ORDER BY IdSensores DESC LIMIT 1;'
        
        self.cursor.execute(sql,val)
        self.connection.commit()

    def InsertarVolumenConsumido(self):
        #Inserto el volumen consumido. No puedo hacer un insert porque al comienzo del programa
        #las plantas no han sido regadas por lo que volumen consumido no tiene valor.
        #Una vez regado las plantas hago el update
        volumen = comun.VolumenConsumido()

        sql = 'UPDATE sensores_Zona1 SET Volumen_consumido = %s ORDER BY IdSensor DESC LIMIT 1;'
        
        self.cursor.execute(sql,volumen)
        self.connection.commit()


class Zona2:
    def __init__(self): #Conexion con la base de datos
        self.connection = pymysql.connect(host='localhost', user='root', password='1234', db='Digital_Regar')  #conecta con MySQL
        self.cursor = self.connection.cursor()  #crea el cursor para las peticiones de MySQL
        print("Conexion Zona 2 establecida")
        
    def select_fecha(self): #Función que saca la fecha de riego elegida por el cliente
        sql = 'SELECT Fecha FROM control_zona2 ORDER BY IdControl DESC LIMIT 1'
        try:
            self.cursor.execute(sql)
            fecha = self.cursor.fetchone()

            if (fecha[0] >= min_anterior and fecha[0] <= min_siguiente):  #Intervalo de una hora para el riego
                print("Fecha correcta para el riego.")
                time.sleep(1)
                return True
            else:
                print("Fecha correcta para el riego.")
                time.sleep(1)
                return False

        except Exception as e:
            raise

    def select_humedad_min(self): #Coge el ultimo valor de la humedad de tierra de la zona 1 elegida por el usuario
        sql =  'SELECT Humedad_min FROM control_zona2 ORDER BY IdControl DESC LIMIT 1'

        try:
            self.cursor.execute(sql)
            hum_min = self.cursor.fetchone()
           
            return hum_min[0]
        
        except Exception as e:
            raise

    def update_frecuencia_riego(self): #Coge el ultimo valor de la fecha introducida por el cliente
        sql = 'SELECT Frecuencia FROM control_zona2 ORDER BY IdControl DESC LIMIT 1'
        sql1 = 'UPDATE control_zona2 SET Fecha = %s ORDER BY IdControl DESC LIMIT 1'

        try:
            self.cursor.execute(sql)
            frecuencia = self.cursor.fetchone()
           #La base de datos devuelve un varchar, dependiendo del input que eliga el usuario en la web -> Frecuencia Semanal devuelve 'semanal' Diaro devuelve 'diario'
            if(frecuencia[0] == 'diario'): #Si la frecuencia de riego elegida por el cliente es diario cambia la fecha para el proximo riego
                val = (siguiente_dia)
                self.cursor.execute(sql1,val) #Ejecuta la query
                self.connection.commit()
                return
                
            elif(frecuencia[0] == 'semanal'): #Si la frecuencia de riego elegida por el cliente es semanal cambia la fecha para el proximo riego
                val = (siguiente_semana)
                self.cursor.execute(sql1,val) #Ejecuta la query
                self.connection.commit()
                return
        
        except Exception as e:
            raise

    def comparar_humedad(self):
        print("Humedad de la planta = ",zona2.HumedadTierra())
        if(zona2.HumedadTierra() < zona2.select_humedad_min()):
            print("Humedad de la planta correcta para regar.")
            time.sleep(1)
            return True
        else:
            print("Humedad de la planta suficiente, no hacce falta regar.")
            time.sleep(1)
            return False
        
    def setup(self):
        GPIO.setmode(GPIO.BCM)       
        GPIO.setup(pinBomba2, GPIO.OUT)
        GPIO.output(pinBomba2, GPIO.LOW)
    
    def activar_bomba(self):      
        zona2.setup()
        while True:
            print ("Bomba activada.")
            GPIO.output(pinBomba2, GPIO.LOW) # luz verde encendida
            time.sleep(3)                
            print ("Bomba desactivada")
            GPIO.output(pinBomba2, GPIO.HIGH) # Nivel alto, el relé está activo
            time.sleep(5)                  # El contacto normalmente abierto está energizado, la luz roja está encendida
	
    def control_bomba(self):
        val = comun.VolumenTanqueInicial()
        print("Volumen tanque inicial: ",val)
        if(val > (0.20)): #Si tiene suficiente agua el tanque entra en la condicion
            print("Volumen suficiente en el tanque.")
            while(zona2.comparar_humedad() == True and zona2.select_fecha() == True):
                print("Regando..")
                zona2.activar_bomba() #Activa la bomba
                zona2.update_frecuencia_riego() # Cambia la fecha para el proximo riego al acabar de regar
                val2 = comun.VolumenTanqueFinal()
                volumen = val - val2
                print("Volumen consumido en el riego = ",volumen) 
                sql = 'UPDATE sensores_Zona2 SET Volumen_consumido = %s ORDER BY IdSensores DESC LIMIT 1;'
                self.cursor.execute(sql,volumen)
                self.connection.commit()
                time.sleep(10)
        else:
            zona2.update_frecuencia_riego()
            return
        
       
        print("Fecha para el siguiente riego actualiazdo.")
                               
      

    def HumedadTierra(self): #Coge el valor del sensor de humedad de la tierra

        adc = MCP3008(0)
        humedad = adc.read( channel = 1 ) #Recoge el valor del  sensor
        #valor  = (-0.147)*(humedad) + 147.1 #Convertimos el valor entre 0-100
        valor  = (-0.17)*(humedad) + 171.4
        return valor
        

    def InsertarValores(self):
        #Inserta los valores de Humedad_tierra y Volumen_tanque a la bbdd
        Volumen = comun.VolumenTanqueInicial()
        HumedadTierra = zona2.HumedadTierra()
        sql = 'INSERT INTO sensores_Zona2(Humedad_tierra,Volumen_tanque) VALUES (%s,%s)'
        val = (HumedadTierra,Volumen)    
        self.cursor.execute(sql,val)
        self.connection.commit()
        zona2.Update_sensorAmbiente()
        

    def Update_sensorAmbiente(self):
        #Inserta el valor del sensor ambiente a la bbdd, hago un update ya que el sensor me devuelve dos valores para una misma variable, por lo que no me deja hacerle un insert
        #Por lo que hago el insert y luego el update de la ultima fila
        #val = comun.SensorAmbiente()
        val = (30,25)
        sql = 'UPDATE sensores_Zona2 SET Humedad_ambiente = %s, Temperatura_ambiente = %s ORDER BY IdSensores DESC LIMIT 1;'
        
        self.cursor.execute(sql,val)
        self.connection.commit()

    def InsertarVolumenConsumido(self):
        #Inserto el volumen consumido. No puedo hacer un insert porque al comienzo del programa las plantas no han sido regadas por lo que volumen consumido no tiene valor.
        #Una vez regado las plantas hago el update
        volumen = comun.VolumenConsumido()

        sql = 'UPDATE sensores_Zona2 SET Volumen_consumido = %s ORDER BY IdSensor DESC LIMIT 1;'
        
        self.cursor.execute(sql,volumen)
        self.connection.commit()


class ValoresComunes:
    def SensorAmbiente(self):      
        #Obtiene el valor de Temperatura_ambiente y Humeda_ambiente del sensor
        #La variable sensor devuleve dos valores
        sensor = Adafruit_DHT.DHT11
        GPIO.setmode(GPIO.BCM)
        pin = 4

        humedad, temperatura = Adafruit_DHT.read_retry(sensor, pin)
        
        return humedad, temperatura

    def VolumenTanqueInicial(self):
        #Obtiene el volumen del tanque
        GPIO.setmode(GPIO.BCM)
        trigger = 18
        echo = 24
        
        GPIO.setup(trigger,GPIO.OUT)
        GPIO.setup(echo,GPIO.IN)
        
        GPIO.output(trigger, True)
        time.sleep(0.00001)
        GPIO.output(trigger, False)
        
        while GPIO.input(echo) == False:
            start = time.time()
        
        while GPIO.input(echo) == True:
            end = time.time()
        
        sig_time = end-start        
        #CM:
        distancia = sig_time / 0.000058
        
        Volumen = ( (3.14 * pow(4.75,2)) * (16 - distancia) ) #Calcular el volumen del tanque
        VolumenLitros = Volumen / 1000 # Convertirlo en litros
  
        #print('Volumen: {} centimeters'.format(VolumenListros))
        
        return VolumenLitros
    
    def VolumenTanqueFinal(self):
        #Misma funcion para el volumen del tanque, pero esta funcion sera llamada al acabar el riego
        trigger = 18
        echo = 24
        
        GPIO.setup(trigger,GPIO.OUT)
        GPIO.setup(echo,GPIO.IN)
        
        GPIO.output(trigger, True)
        time.sleep(0.00001)
        GPIO.output(trigger, False)
        
        while GPIO.input(echo) == False:
            start = time.time()
        
        while GPIO.input(echo) == True:
            end = time.time()
        
        sig_time = end-start        
        #CM:
        distancia = sig_time / 0.000058
        
        Volumen = ( (3.14 * pow(4.75,2)) * (16 - distancia) ) #Calcular el volumen del tanque
        VolumenLitros = Volumen / 1000 # Convertirlo en litros
  
        #print('Volumen: {} centimeters'.format(VolumenListros))

        return VolumenLitros


    def VolumenConsumido(self):
        #Resta los dos volumenes para conseguir el volumen consumido del riego
        Volumen = comun.VolumenTanqueInicial() - comun.VolumenTanqueFinal

        return Volumen

zona1 = Zona1()
zona2 = Zona2()
comun = ValoresComunes()


while True:
    print("\n")
    print("----- Zona 1 -----")
    print("Insertando valores de los sensores...")
    zona1.InsertarValores()  #Inserta los valores de Humedad_tierra y Volumen_tanque a la bbdd
    time.sleep(2)
    print("Se han insertado de forma correcta.")
    print("\n") 
    time.sleep(5)
    print("Iniciando control de bomba...")
    zona1.control_bomba() #Inicio el logaritmo de control de la bomba
    print("Se ha regado de forma exitosa.")
    print("\n") 
    print("----- Zona 2 -----")
    print("Insertando valores de los sensores...")
    zona2.InsertarValores()
    time.sleep(5)
    print("Se han insertado de forma correcta.")
    print("\n") 
    time.sleep(2)
    print("Iniciando control de bomba...")
    zona2.control_bomba()
    print("Se ha regado de forma exitosa.")
    time.sleep(1800)