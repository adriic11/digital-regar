from ast import While
import pymysql
import time
from datetime import datetime, timedelta
import Adafruit_DHT
import RPi.GPIO as GPIO
import math
import busio
import digitalio
import board

dia_ahora = datetime.now()

pinBomba1 = 21
pinBomba2 = 20

pinHumedad1 = 26
pinHumedad2 = 16

GPIO.setmode(GPIO.BCM)
GPIO.setup(pinHumedad1, GPIO.IN)

class Zona1:
    def __init__(self): #Conexion con la base de datos
        self.connection = pymysql.connect(host='localhost', user='root', password='1234', db='Digital_Regar')  #conecta con MySQL
        self.cursor = self.connection.cursor()  #crea el cursor para las peticiones de MySQL
        print("Conexion Zona 1 establecida")    
    
    def setup(self):
        GPIO.setmode(GPIO.BCM)       
        GPIO.setup(pinBomba1, GPIO.OUT)
        GPIO.output(pinBomba1, GPIO.LOW)
        
    def activar_bomba(self):
        
        zona1.setup()
        
        print ("Bomba activada.")
        GPIO.output(pinBomba1, GPIO.LOW) # luz verde encendida
        time.sleep(5)                
        print ("Bomba desactivada.")
        GPIO.output(pinBomba1, GPIO.HIGH) # Nivel alto, el relé está activo
        time.sleep(5)                  # El contacto normalmente abierto está energizado, la luz roja está encendida
                                

    def control_bomba(self):
        humedad = GPIO.input(26)
        print("Volumen tanque inicial = ",comun.VolumenTanque())
        time.sleep(2)
        while(humedad == 1): #Se encuentra en estado seco
            if(comun.VolumenTanque() > (0.20)): #Si tiene suficiente agua el tanque entra en la condicion
                val = comun.VolumenTanque()
                print("Volumen suficiente en el tanque.")
                time.sleep(2)
                print("Regando..")
                zona1.activar_bomba() #Activa la bomba
                val2 = comun.VolumenTanque()
                val -= val2
                print("Volumen consumido en el riego = ",val)
                sql = 'UPDATE sensores_Zona1 SET Volumen_consumido = %s ORDER BY IdSensores DESC LIMIT 1;'
                self.cursor.execute(sql,volumen)
                self.connection.commit()
                zona1.FechaRiego()
            else:
                 print("Volumen insuficiente en el tanque.")
             
      
    def FechaRiego(self):
        sql = 'INSERT INTO datos_Zona1(Fecha) VALUES (%s)'
        val = (dia_ahora)    
        self.cursor.execute(sql,val)
        self.connection.commit()

    def InsertarValores(self):
        #Inserta los valores de Humedad_tierra y Volumen_tanque a la bbdd
        Volumen = comun.VolumenTanqueInicial()
        HumedadTierra = zona1.HumedadTierra()
        sql = 'INSERT INTO sensores_Zona1(Volumen_tanque) VALUES (%s)'
        
        val = (Volumen)
        self.cursor.execute(sql,val)
        self.connection.commit()

        zona1.Update_sensorAmbiente()
        

    def Update_sensorAmbiente(self):
        #Inserta el valor del sensor ambiente a la bbdd, hago un update ya que el sensor me devuelve dos valores para una misma variable, por lo que no me deja hacerle un insert
        #Por lo que hago el insert y luego el update de la ultima fila
        val = comun.SensorAmbiente()
        
        sql = 'UPDATE sensores_Zona1 SET Humedad_ambiente = %s, Temperatura_ambiente = %s ORDER BY IdSensores DESC LIMIT 1;'
        
        self.cursor.execute(sql,val)
        self.connection.commit()


class Zona2:
    def __init__(self): #Conexion con la base de datos
        self.connection = pymysql.connect(host='localhost', user='root', password='1234', db='Digital_Regar')  #conecta con MySQL
        self.cursor = self.connection.cursor()  #crea el cursor para las peticiones de MySQL
        print("Conexion Zona 2 establecida")
        
    def setup(self):
        GPIO.setmode(GPIO.BCM)       
        GPIO.setup(pinBomba2, GPIO.OUT)
        GPIO.output(pinBomba2, GPIO.LOW)
    
    def activar_bomba(self):      
        zona2.setup()
        while True:
            print ("Bomba activada.")
            GPIO.output(pinBomba2, GPIO.LOW) # luz verde encendida
            time.sleep(5)                
            print ("Bomba desactivada")
            GPIO.output(pinBomba2, GPIO.HIGH) # Nivel alto, el relé está activo
            time.sleep(5)                  # El contacto normalmente abierto está energizado, la luz roja está encendida
	
    def control_bomba(self):
        humedad = GPIO.input(16)
        print("Volumen tanque inicial = ",comun.VolumenTanque())
        time.sleep(2)
        while(humedad == 1): #Se encuentra en estado seco
            if(comun.VolumenTanque() > (0.20)): #Si tiene suficiente agua el tanque entra en la condicion
                val = comun.VolumenTanque()
                print("Volumen suficiente en el tanque.")
                time.sleep(2)
                print("Regando..")
                zona1.activar_bomba() #Activa la bomba
                val2 = comun.VolumenTanque()
                val -= val2
                print("Volumen consumido en el riego = ",val)
                sql = 'UPDATE sensores_Zona2 SET Volumen_consumido = %s ORDER BY IdSensores DESC LIMIT 1;'
                self.cursor.execute(sql,volumen)
                self.connection.commit()
                zona2.FechaRiego()
                
            else:
                 print("Volumen insuficiente en el tanque.")

    def FechaRiego(self):
        sql = 'INSERT INTO datos_Zona2(Fecha) VALUES (%s)'
        val = (dia_ahora)    
        self.cursor.execute(sql,val)
        self.connection.commit()                 
        

    def InsertarValores(self):
        #Inserta los valores de Humedad_tierra y Volumen_tanque a la bbdd
        Volumen = comun.VolumenTanqueInicial()
        HumedadTierra = zona2.HumedadTierra()
        sql = 'INSERT INTO sensores_Zona2(Volumen_tanque) VALUES (%s)'
        val = (Volumen)    
        self.cursor.execute(sql,val)
        self.connection.commit()
        zona2.Update_sensorAmbiente()
        

    def Update_sensorAmbiente(self):
        #Inserta el valor del sensor ambiente a la bbdd, hago un update ya que el sensor me devuelve dos valores para una misma variable, por lo que no me deja hacerle un insert
        #Por lo que hago el insert y luego el update de la ultima fila
        val = comun.SensorAmbiente()
     
        sql = 'UPDATE sensores_Zona2 SET Humedad_ambiente = %s, Temperatura_ambiente = %s ORDER BY IdSensores DESC LIMIT 1;'
        
        self.cursor.execute(sql,val)
        self.connection.commit()




class ValoresComunes:
    def SensorAmbiente(self):      
        #Obtiene el valor de Temperatura_ambiente y Humeda_ambiente del sensor
        #La variable sensor devuleve dos valores
        sensor = Adafruit_DHT.DHT11
        GPIO.setmode(GPIO.BCM)
        pin = 4

        humedad, temperatura = Adafruit_DHT.read_retry(sensor, pin)
        
        return humedad, temperatura

    def VolumenTanque(self):
        #Obtiene el volumen del tanque
        GPIO.setmode(GPIO.BCM)
        trigger = 18
        echo = 24
        
        GPIO.setup(trigger,GPIO.OUT)
        GPIO.setup(echo,GPIO.IN)
        
        GPIO.output(trigger, True)
        time.sleep(0.00001)
        GPIO.output(trigger, False)
        
        while GPIO.input(echo) == False:
            start = time.time()
        
        while GPIO.input(echo) == True:
            end = time.time()
        
        sig_time = end-start        
        #CM:
        distancia = sig_time / 0.000058
        
        Volumen = ( (3.14 * pow(4.75,2)) * (16 - distancia) ) #Calcular el volumen del tanque
        VolumenLitros = Volumen / 1000 # Convertirlo en litros

        VolumenFinal = round(VolumenLitros,2)
        #print('Volumen: {} centimeters'.format(VolumenListros))
        
        return VolumenFinal

    


zona1 = Zona1()
zona2 = Zona2()
comun = ValoresComunes()


while True:
    print("\n")
    print("----- Zona 1 -----")
    print("Insertando valores de los sensores...")
    zona1.InsertarValores()  #Inserta los valores de Humedad_tierra y Volumen_tanque a la bbdd
    time.sleep(2)
    print("Se han insertado de forma correcta.")
    print("\n") 
    time.sleep(5)
    print("Iniciando control de bomba...")
    zona1.control_bomba() #Inicio el logaritmo de control de la bomba
    print("Se ha regado de forma exitosa.")
    print("\n")
    
    print("----- Zona 2 -----")
    print("Insertando valores de los sensores...")
    zona2.InsertarValores()
    time.sleep(5)
    print("Se han insertado de forma correcta.")
    print("\n") 
    time.sleep(2)
    print("Iniciando control de bomba...")
    zona2.control_bomba()
    print("Se ha regado de forma exitosa.")
    time.sleep(1800)
