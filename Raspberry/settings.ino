String comando;

#define sensorPin A0
#define fuente 8

void setup() {
	Serial.begin(9600);   							//Comunicacion serail -> El mismo en arduino y rasberry
	pinMode(fuente, OUTPUT);						//Envia el comando
}

void loop() {
	int leer = analogRead(sensorPin);				//Coge el valor del sensor y lo retorna en temperatura
	float voltage = leer * 0.5;								
	voltage /= 1024.0;
	float temperaturaC = (voltage - 0.5) * 100; 	//Temperatura en Cº

	Serail.println(temperaturaC);					//Dato que se va a enviar a la Rasberry

	if(Serial.available()) {						//Empieza a escuchar la comunicación
		comando = Serail.readStringUntil('\n');		//Coge el valor del string que contenga el salto de linea
		comando.trim();								//Por si hay algún espacio de linea adicional
		if (comando.equals("on")) {					//Si el comando es ON, enciende la bomba
			digitalWrite(fuente, HIGH);
		}
		else if (comando.equals("off")) {			//Si el comando es off, la apaga
			digitalWrite(fuente, LOW);
		}
		else {										//Soluciona problemas: Por si en la query no se coge
			digitalWrite(fuente, HIGH);				//ningun valor ON  u OFF, enciende la bomba medio segundo
			delay(500);								//y la apaga
			digitalWrite(fuente, LOW);
		}
	}
	delay(1000);									//Espera un segundo y vuelve a hacer el loop
}