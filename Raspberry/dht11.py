import Adafruit_DHT
import time

sensor = Adafruit_DHT.DHT11
pin = 4
while True:
     humedad, temperatura = Adafruit_DHT.read_retry(sensor, pin)
     print('Temp={0:0.1f}*C Humidity={1:0.1f}%'.format(temperatura, humedad))
     time.sleep(10)