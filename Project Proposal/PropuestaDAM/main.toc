\babel@toc {spanish}{}
\contentsline {section}{\numberline {1}Introducci\'on}{2}{section.1}
\contentsline {section}{\numberline {2}Investigaci\'on sobre cultivo de plantas de interior}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Factores ambientales}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Nutrientes}{3}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Irrigaci\'on}{3}{subsection.2.3}
\contentsline {section}{\numberline {3}Problemas a resolver}{4}{section.3}
\contentsline {section}{\numberline {4}Arquitectura del sistema}{5}{section.4}
\contentsline {subsection}{\numberline {4.1}Descripci\'on general}{5}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Descripci\'on del Hardware}{6}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Descripci\'on del software}{7}{subsection.4.3}
